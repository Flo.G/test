## Test Novaway

### Déroulement des test 

Pour la partie html, j'ai commencé par m'occuper du positionnement (responsivité) des objets avant d'intégrer le contenu. Pour le positionnement je me suis appuyé sur Bootstrap avec son système de "col" et de row.
Je suis donc parti sur 3 grands blocs: 

Le premier qui contiendra l'image. 
Le deuxième où il y aura le parti download.
Le troisième bloc contiendra deux autres blocs:
Un pour les conseils et l'autre pour la notation.

Une fois ces blocs créer et responsives j'ai intègré le contenu.

Pour la compatibilité, j'ai regardé via des sites et notamment les balises non lisibles par certains navigateurs.

Pour les animations (en css), j'ai intégré un petit système de notation en étoile qui pourra notamment évoluer grace au javascript pour aussi un compteur pour faire augmenter ou diminuer la notation 

Pour la partie javascript j'ai inséreré la logique du code en commentaire dans le ficher afin de permettre d'avoir un code compréhensible pour moi ainsi que les personnes qui peuvent le ré-utiliser.

Pour cette partie je n'ai pu aller au bout de l'exercice mais j'ai essayer d'aller le plus loin possible.


